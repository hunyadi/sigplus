# sigplus #

sigplus adds photo and multimedia galleries to a Joomla article with a simple syntax. It takes a matter of minutes to set up but comes with a vast range of options for advanced users.

Features:

* user-selectable pop-up window engine, including [boxplus](https://hunyadi.info.hu/projects/boxplusx/) (specifically designed for sigplus), Bootstrap Modal (included with Joomla), [Fancybox](https://fancyapps.com/fancybox/), [Fancybox3](https://fancyapps.com/fancybox/3/) and [Slimbox2](https://www.digitalia.be/software/slimbox2/)
* user-selectable image slider/carousel/rotator
* free-flow and grid layout mode; row, column and grid arrangement
* fully responsive, mobile-enabled
* build gallery from images on the server or photos on Flickr
* support for image types .jpg, .png and .gif (with and without transparency, with and without animation)
* multimedia support (HTML <video> tag)
* best-fit thumbnail generation with automatic cropping and centering
* progressive load feature to save network bandwidth: only those images are fetched from the server that are to be shown
* multiple galleries per content item and per page
* multilingual, search engine-friendly image labels and more verbose description set globally or for each individual image
* right-to-left language support
* image caption templates
* image metadata processing; IPTC and EXIF data extraction
* download option to save high-resolution image version
* custom styling (e.g. preview image margin, border, padding and opacity, slide duration and animation delay)
* custom sort criteria (user-defined, file name, last modification time and random order)
* integrates into Joomla content editor and Joomla search
* large gallery support (100 or more images in a single gallery)
* 100% CSS3 and JavaScript, HTML5-enabled
* default global settings for the entire site and local parameter overrides for individual galleries
* restricted-access galleries
* translated to several languages
* and more...

### Technical requirements ###

* Joomla 3.0 or later
* GD2 (Graphics Draw) or ImageMagick or GraphicsMagick PHP extension
* compatible with both MySQL and Microsoft SQL Server
