# jbuild Custom build system for Joomla!
# Copyright 2009-2018 Levente Hunyadi <https://hunyadi.info.hu>
#
# jbuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# jbuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Requires Python 3.x or later

import io
import errno
import json
import os
import os.path
import platform
import re
import shutil
import subprocess
import tarfile
import time
import xml.etree.ElementTree as ElementTree
import zipfile
import zlib

positive_extension_preference = ['.html','.svg','.css','.xml','.ini','.txt','.php','.sql','.js']  # prefer to be first
negative_extension_preference = ['.gif','.png']  # prefer to be last

zlib.Z_DEFAULT_COMPRESSION = 9  # set zlib compression level to maximum

def find_in_path(file, path=None):
    """find_in_path(file[, path=os.environ['PATH']]) -> list

    Finds all files with a specified name that exist in the operating system's
    search path (os.environ['PATH']), and returns them as a list in the same
    order as the path.  Instead of using the operating system's search path,
    the path argument can specify an alternative path, either as a list of paths
    of directories, or as a single string seperated by the character os.pathsep.

    If you want to limit the found files to those with particular properties,
    use filter() or which()."""

    if path is None:
        path = os.environ.get('PATH', '')
    if type(path) is type(''):
        path = path.split(os.pathsep)
    if platform.system() == 'Windows':
        path.extend(map(lambda d: os.path.join(os.environ.get('WINDIR'), d), ['System','System32','SysWoW64']))
    return filter(os.path.isfile, map(lambda d: os.path.join(d, file), path))

def which(file, mode=os.F_OK | os.X_OK, path=None):
    """which(file[, mode][, path=os.environ['PATH']]) -> list

    Finds all executable files in the operating system's search path
    (os.environ['PATH']), and returns them as a list in the same order as the
    path.  Like the UNIX shell command 'which'.  Instead of using the operating
    system's search path, the path argument can specify an alternative path,
    either as a list of paths of directories, or as a single string seperated by
    the character os.pathsep.

    Alternatively, mode can be changed to a different os.access mode to
    check for files or directories other than "executable files".  For example,
    you can additionally enforce that the file be readable by specifying
    mode = os.F_OK | os.X_OK | os.R_OK."""

    return filter(lambda path: os.access(path, mode), find_in_path(file, path))

def find_executable(executable):
    pathext = os.environ['PATHEXT'].lower().split(os.pathsep)
    (base, ext) = os.path.splitext(executable)
    if pathext and not ext:
        paths = (path for ext in pathext for path in find_in_path(executable + ext))
    else:
        paths = find_in_path(executable)
    try:
        return next(paths)
    except StopIteration:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), executable)

def find_java():
    java_home = os.environ.get('JAVA_HOME')
    if java_home:
        return os.path.join(java_home, 'bin', 'java')
    else:
        return find_executable('java')

def as_int(arg):
    """as_int(string) -> int

    Convert string to integer, returning None for input None."""

    if arg is None:
        return None
    else:
        return int(arg)

class Version:
    """Version information on a product in the format "x.y.z.w" where
    * x is the major version,
    * y is the minor version,
    * z is the revision number and
    * w is the incremental build number."""

    def __init__(self, majorversion, minorversion = None, maintenance = None, build = None):
        self.majorversion = majorversion
        self.minorversion = minorversion
        self.maintenance = maintenance
        self.build = build

    def as_list(self):
        version = [self.majorversion]
        if self.minorversion is not None:
            version.append(self.minorversion)
            if self.maintenance is not None:
                version.append(self.maintenance)
                if self.build is not None:
                    version.append(self.build)
        return [str(elem) for elem in version]

    def __str__(self):
        version = self.as_list()
        version[-1:] = []
        return '.'.join(version)

    @staticmethod
    def from_string(text):
        m = re.search(r'(\d+)(?:[.](\d+)(?:[.](\d+)(?:[.](\d+))?)?)?', text)
        if m:
            majorversion = as_int(m.group(1))
            minorversion = as_int(m.group(2))
            maintenance = as_int(m.group(3))
            build = as_int(m.group(4))
            return Version(majorversion, minorversion, maintenance, build)
        else:
            return None

    def as_string(self):
        return '.'.join(self.as_list())

    def as_time(self):
        if self.minorversion is None:
            minorversion = 0
        else:
            minorversion = self.minorversion
        if self.maintenance is None:
            maintenance = 0
        else:
            maintenance = self.maintenance
        t = time.localtime()
        t = (t.tm_year,t.tm_mon,t.tm_mday,self.majorversion,minorversion,maintenance,t.tm_wday,t.tm_yday,t.tm_isdst);
        return time.mktime(t)

# map extensions to a numeric preference index (the higher the value, the more preferred)
extension_preference = {}
for index, item in enumerate(reversed(positive_extension_preference)):
    extension_preference[item] = (index + 1)
for index, item in enumerate(negative_extension_preference):
    extension_preference[item] = -(index + 1)

def file_sort_key(path):
    (basename,extension) = os.path.splitext(path)
    return (os.path.isdir(path), extension_preference.get(extension, 0))

class Packager:
    """Builds a distribution package of files in a folder."""

    leading_whitespace_regex = re.compile(r"^[\t ]+", re.MULTILINE | re.UNICODE)

    @staticmethod
    def replace_whitespace(match):
        """Replaces leading whitespace with tab characters."""

        tab_length = 4
        tab_count = match.group(0).count('\t') + match.group(0).count(' ') // tab_length
        return '\t' * tab_count

    def __init__(self, basepath, packagename, version, folder = None):
        # version information
        self.version = version
        # distribution archive
        self.basepath = basepath
        # last modification time for files
        self.mtime = version.as_time();

    def __del__(self):
        self.close()

    def close(self):
        self.archive.close()
        if hasattr(self, 'fileobj') and self.fileobj:
            self.fileobj.close()

    def get_file_contents(self, fullpath):
        """Opens a text file and replaces version placeholders."""

        with open(fullpath, 'r', encoding='utf-8') as f:
            t = f.read()
            t = t.replace('$__VERSION__$', str(self.version))
            t = t.replace('$__VERSION_NUMBER__$', self.version.as_string())
            t = t.replace('$__DATE__$', time.strftime("%b %d, %Y", time.gmtime()))
            t = re.sub(type(self).leading_whitespace_regex, type(self).replace_whitespace, t)
            return t.encode('utf-8')

    def is_text(self, fullpath):
        """Checks if version placeholders should be replaced in a file."""

        (basename,extension) = os.path.splitext(fullpath)
        return extension in ['.css','.html','.ini','.js','.php','.sql','.svg','.txt','.xml']

    @staticmethod
    def create(basepath, packagename, version, folder):
        if packagename.endswith(('.tar','.tgz','.tar.gz')):
            return TarPackager(basepath, packagename, version, folder)
        else:
            return ZipPackager(basepath, packagename, version, folder)

class TarPackager(Packager):
    def __init__(self, basepath, packagename, version, folder = None):
        Packager.__init__(self, basepath, packagename, version, folder)
        if packagename.endswith(('.tar.gz','.tgz')):
            mode = 'w:gz'
            packagefilename = packagename
        elif packagename.endswith('.tar'):
            mode = 'w'
            packagefilename = packagename
        else:
            mode = 'w:gz'
            packagefilename = packagename + '.tgz'
        if folder:
            self.fileobj = open(os.path.join(folder, packagefilename), 'wb')
        else:
            self.fileobj = None
        self.archive = tarfile.open(packagefilename, mode, format=tarfile.GNU_FORMAT, fileobj=self.fileobj)

    def add(self, relpath, fullpath = None):
        if not fullpath:
            fullpath = os.path.join(self.basepath, relpath)
        info = self.archive.gettarinfo(fullpath)
        info.name = relpath;
        info.mtime = self.mtime;
        if info.isfile():
            if self.is_text(fullpath):
                b = self.get_file_contents(fullpath)
                info.size = len(b)
                with io.BytesIO(b) as s:
                    self.archive.addfile(info, s)
            else:
                with open(fullpath, 'rb') as f:
                    self.archive.addfile(info, f)
        elif info.isdir():
            self.archive.addfile(info)

class ZipPackager(Packager):
    def __init__(self, basepath, packagename, version, folder = None):
        Packager.__init__(self, basepath, packagename, version, folder)
        if packagename.endswith(('.ar.zip','.store.zip')):
            self.compression = zipfile.ZIP_STORED  # no compression
            packagefilename = packagename
        elif packagename.endswith('.zip'):
            self.compression = zipfile.ZIP_DEFLATED
            packagefilename = packagename
        else:
            self.compression = zipfile.ZIP_DEFLATED
            packagefilename = packagename + '.zip'
        if folder:
            packagefilename = os.path.join(folder, packagefilename)
        self.archive = zipfile.ZipFile(packagefilename, 'w', self.compression)

    def add(self, relpath, fullpath = None):
        if not fullpath:
            fullpath = os.path.join(self.basepath, relpath)
        if not os.path.isdir(fullpath):
            info = zipfile.ZipInfo(relpath, time.gmtime(self.mtime)[0:6])
            info.compress_type = self.compression
            if self.is_text(fullpath):
                b = self.get_file_contents(fullpath)
                self.archive.writestr(info, b)
            else:
                with open(fullpath, 'rb') as f:
                    self.archive.writestr(info, f.read())

def package(basepath, packagename, version, folder = None):
    """Package source files into a distribution with overrides."""

    def package_list():
        """Get list of folder and file absolute paths for to be packaged."""

        fullpaths = []
        for (path, folders, files) in os.walk(basepath):
            folders[:] = [folder for folder in folders if not folder.startswith('.')]  # skip hidden folders, e.g. ".svn"
            files[:] = [file for file in files if not file.startswith('.')]  # skip hidden file, e.g. ".gitignore"
            fullpaths.extend([os.path.join(path, folder) for folder in folders])
            fullpaths.extend([os.path.join(path, file) for file in files])
        return fullpaths

    packager = Packager.create(basepath, packagename, version, folder)
    fullpaths = package_list()
    fullpaths.sort(key=file_sort_key, reverse=True)
    for fullpath in fullpaths:
        relpath = os.path.relpath(fullpath, basepath)
        # print(relpath + ' added')
        packager.add(relpath)
    packager.close()

def parse_xml(fullpath):
    return ElementTree.parse(fullpath)

def write_xml(fullpath, dom):
    dom.write(fullpath, encoding='utf-8', xml_declaration=True)

def check_languages(basepath, packagexml):
    fullpath = os.path.join(basepath, packagexml)
    (basename,extension) = os.path.splitext(packagexml)

    dom = parse_xml(fullpath)
    if dom:
        languagesnode = dom.find('languages')
        if languagesnode:
            languagefolder = languagesnode.attrib.get('folder')
            languages = list_languages(os.path.join(basepath, languagefolder))

            # check if all language files in the file system have a corresponding entry in the extension XML
            for (language,country,name) in languages:
                code = '{0}-{1}'.format(language, country)
                languagenode = languagesnode.find("language[@tag='{0}']".format(code))
                if languagenode is None:
                    raise KeyError(code)

            # check if all entries in the extension XML have a corresponding language file in the file system
            for languagenode in languagesnode:
                if not os.path.exists(os.path.join(basepath, languagefolder, languagenode.text)):
                    raise KeyError(languagenode.text)

def list_languages(basepath):
    """List language localization files in a directory."""
    languagelist = []
    for foldername in os.listdir(basepath):
        foldermatch = re.match(r'^([a-z]{2})-([A-Z]{2})$', foldername)
        if foldermatch:
            for entryname in os.listdir(os.path.join(basepath, foldername)):
                entrymatch = re.match(r'^([a-z]{2})-([A-Z]{2})\.(.*)\.ini$', entryname)
                if entrymatch:
                    language = entrymatch.group(1)
                    country = entrymatch.group(2)
                    name = entrymatch.group(3)
                    languagelist.append( (language,country,name) )
    return languagelist

def get_version(basepath, packagexml, update=False):
    """Fetch and update version number."""

    t = False
    fullpath = os.path.join(basepath, packagexml)
    with open(fullpath, 'r', encoding='utf-8') as f:
        t = f.read()
        m = re.search(r'<version>(\d+)(?:[.](\d+)(?:[.](\d+)(?:[.](\d+))?)?)?</version>', t)
        if m:
            majorversion = as_int(m.group(1))
            minorversion = as_int(m.group(2))
            maintenance = as_int(m.group(3))
            build = as_int(m.group(4))
            if not build:
                build = 0
            if update:
                build += 1
            version = Version(majorversion, minorversion, maintenance, build)
            t = t.replace(m.group(0), '<version>' + version.as_string() + '</version>')
    if update and t:
        with open(fullpath, 'w', encoding='utf-8') as f:
            f.write(t)
    return version

def get_version_xml(basepath, packagexml, update=False):
    """Fetch and update version number."""

    fullpath = os.path.join(basepath, packagexml)
    dom = parse_xml(fullpath)

    if dom:
        versionnode = dom.find('version')
        if versionnode:
            version = Version.from_string(versionnode.text)
            if update:
                version.build += 1
                versionnode.text = version.as_string()

        return version
    else:
        return None

def minify_yui(uncompressed, compressed, method):
    sinfo = subprocess.STARTUPINFO()
    # sinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    subprocess.check_call([find_java(),'-jar',os.path.join(os.path.split(__file__)[0],'yuicompressor.jar'),'-v','--charset','utf8','-o',compressed,uncompressed], startupinfo=sinfo)

def minify_google(uncompressed, compressed, method):
    sinfo = subprocess.STARTUPINFO()
    # sinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    pattern = re.compile(r'/\*(\{[^{}]*\})\*/')
    with open(uncompressed, 'r', encoding='utf-8') as f:
        match = pattern.search(f.read())
        if match:
            options = json.loads(match.group(1))
            if options.get('compilation_level') == 'ADVANCED_OPTIMIZATIONS':
                method = 'advanced'

    params = ['--language_in','ECMASCRIPT6_STRICT','--language_out','ECMASCRIPT5_STRICT']
    if method == 'advanced':
        params.extend(['--compilation_level','ADVANCED_OPTIMIZATIONS','--isolation_mode','IIFE','--warning_level','VERBOSE'])
        warnings = ['accessControls','checkDebuggerStatement','checkRegExp','constantProperty','const','deprecatedAnnotations','deprecated','missingReturn','strictModuleDepCheck','typeInvalidation','undefinedNames','visibility']
        params.extend(['--jscomp_warning={0}'.format(warning) for warning in warnings])
    else:
        params.extend(['--compilation_level','SIMPLE_OPTIMIZATIONS'])

    subprocess.check_call([find_java(),'-jar',os.path.join(os.path.split(__file__)[0],'closure-compiler.jar'),'--js',uncompressed,'--js_output_file',compressed,'--charset','UTF-8'] + params, startupinfo=sinfo)

def minify_microsoft(uncompressed, compressed, method):
    sinfo = subprocess.STARTUPINFO()
    # sinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    subprocess.check_call([os.path.join(os.path.split(__file__)[0],'AjaxMin.exe'),'-analyze','-clobber:true','-literals:combine',uncompressed,'-out',compressed], startupinfo=sinfo)

def minify_file(path, f, minify_method = minify_yui, compression_method = 'simple'):
    (basename,extension) = os.path.splitext(f)
    uncompressed = os.path.join(path, f)
    compressed = os.path.join(path, basename + '.min' + extension)
    if os.path.exists(compressed):
        if os.path.getmtime(uncompressed) > os.path.getmtime(compressed):
            print(f + ' has changed')
            minify_method(uncompressed, compressed, compression_method)
        else:
            print(f + ' is up-to-date')

def minify(basepath, method = 'simple'):
    """Minify javascript files."""
    for (path, folders, files) in os.walk(basepath):
        for f in files:
            (basename,extension) = os.path.splitext(f);
            if extension == '.js':
                minify_file(path, f, minify_google, method)
            elif extension == '.css':
                minify_file(path, f, minify_yui, method)

if __name__ == '__main__':
    print('Custom build system for Joomla! 3.0\nCopyright 2009-2017 Levente Hunyadi <https://hunyadi.info.hu>')
    print('This file is meant to be used as a Python module and not run directly\nfrom the command line.')
